import React, {Component} from 'react';
import PadContainer from './PadContainer';
import './style/css/app.css';
//Audio
import bassDrum1 from './assets/sounds/mattel-bd1.wav'; 
import hiHat2 from './assets/sounds/mattel-hh2.wav';
import hiHatOpen1 from './assets/sounds/mattel-hho1.wav';
import perc1 from './assets/sounds/mattel-per1.wav';
import snareDrum4 from './assets/sounds/mattel-sd4.wav';
import tomDrum16 from './assets/sounds/mattel-tom16.wav';
import trigg from './assets/sounds/mattel-trigg.wav';
import liquidChords from './assets/sounds/liquid-chords.wav';
import { pad }  from './PadModel';


interface IState {
    pads: pad[],
    keysDown: Set<string>
}
interface IProps{

}
class Board extends Component<IProps,IState> {
    constructor( props: any ) {
        super( props );
        this.state = {
            //Drop down menu for sample file would be kind of cool? Could maybe put like a drop down box on it
            pads : [
                { id:1, isLoop:false, isHit:false, hotKey:'q', sampleFile:bassDrum1 },
                { id:2, isLoop:false, isHit:false, hotKey:'w', sampleFile:hiHat2 },
                { id:3, isLoop:false, isHit:false, hotKey:'e', sampleFile:hiHatOpen1 },
                { id:4, isLoop:false, isHit:false, hotKey:'r', sampleFile:perc1 },
                { id:5, isLoop:false, isHit:false, hotKey:'t', sampleFile:snareDrum4 },
                { id:6, isLoop:false, isHit:false, hotKey:'y', sampleFile:tomDrum16 },
                { id:7, isLoop:false, isHit:false, hotKey:'u', sampleFile:trigg },
                { id:8, isLoop:false, isHit:false, hotKey:'i', sampleFile:trigg },
                { id:9, isLoop:false, isHit:false, hotKey:'o', sampleFile:liquidChords}
            ],
            keysDown: new Set(),
        };
    }

    componentDidMount() {
        window.addEventListener('keydown', this.handleKeyDown);
        window.addEventListener('keyup', this.handleKeyUp);
        window.focus();
      }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyDown);
        window.removeEventListener('keyup', this.handleKeyUp);
      }
    
    toggleLoop = (i: number) => {
        const pads = this.state.pads;
        pads[i-1].isLoop =! pads[i-1].isLoop;
        this.setState(
            {pads}
            );

    }

    toggleHit = (i: number) => {    
        const pads = this.state.pads;
        pads[i-1].isHit =! pads[i-1].isHit;
       this.setState(
       {pads}
       );
    }

    handleKeyDown = (event:any)=>{
        const keysDown = this.state.keysDown;
        keysDown.add(event.key);
        this.setState({keysDown})
    }
handleKeyUp = (event:any)=>{
    const keysDown = this.state.keysDown;
    keysDown.delete(event.key);
    this.setState({keysDown})
    }

    render(){
        const {pads} = this.state;
        const {keysDown} = this.state;
    return(
        <div className="App">
        <div className="title"> sampler_io
</div>
        <PadContainer pads = { pads } toggleLoop = { this.toggleLoop } keysDown={ keysDown }/>
        </div>
    );
}
};
export default Board