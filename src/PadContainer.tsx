import React from 'react';
import Pad from './Pad'
import { pad }  from './PadModel';
import './style/css/padContainer.css'

interface PadContainerProps{
    pads: pad[],
    toggleLoop: any,
    keysDown: Set<string>

}
const PadContainer = ( { pads, toggleLoop, keysDown }: PadContainerProps ) => {
    const padsRender: any = pads.map( pad => (
        <Pad 
            key = { pad.id } 
            id = { pad.id }
            isLoop = { pad.isLoop }
            hotKey = { pad.hotKey } 
            sampleFile ={ pad.sampleFile } 
            keysDown = { keysDown }
            toggleLoop = { toggleLoop }
        >
        </Pad>
        ));
    return(
        <div className = "PadContainer">   
            { padsRender } 
        </div>
    );
}

export default PadContainer