import React, { useRef, useEffect, MutableRefObject, RefObject } from 'react';
import './style/css/pad.css'

interface PadProps {
    //unsure of this
    children: never[],
    id: number,
    keysDown: Set<string>,
    hotKey: string,
    toggleLoop: any,
    isLoop: boolean,
    sampleFile: any,

}
const Pad = ( { id, keysDown, hotKey, toggleLoop, isLoop, sampleFile }: PadProps ) => {
//This is a hook!!!
    const nodeRef: MutableRefObject<HTMLAudioElement | null> = useRef<HTMLAudioElement>(null);

    useEffect(() =>{
            if(nodeRef && nodeRef.current){
                if( keysDown.has( hotKey ) || isLoop ){
                    nodeRef.current.play();
                }
                else{            
                    nodeRef.current.pause();
                    nodeRef.current.currentTime = 0
                }
            }

       
    })

    let padIsHitStyling = ()=>{
        if ( keysDown.has( hotKey ) ){
            return "PadIsHit"
        }
        else{
            return "Pad"
        }
    }
    return(
        <div className = { padIsHitStyling() }>
            <p className = "padId">pad # { id }</p>
            <p className = "sampleFile">{sampleFile}</p>
        <audio
        id = "audio"
        ref={ nodeRef }
        controls 
        loop={ isLoop }
        src={ sampleFile }/>
        <button id="loopButton" onClick = { () => toggleLoop( id ) }>loop</button>
        </div>
    );
}
export default Pad;