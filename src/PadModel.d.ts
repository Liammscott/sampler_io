export interface pad {
        id: number,
        isLoop:boolean,
        isHit:boolean,
        hotKey: string,
        sampleFile:any,
}