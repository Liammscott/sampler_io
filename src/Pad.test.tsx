import React from 'react'; 
import { pad }  from './PadModel';
import { shallow, mount } from 'enzyme';
import Pad from './Pad';
import trigg from './assets/sounds/mattel-trigg.wav';


describe('Pad', () => {

  let wrapperPadActive:any;
  let wrapperPadInactive:any;
  let toggleLoop = jest.fn();

  beforeEach(() =>{
    wrapperPadInactive = shallow(<Pad
      children = {[]}
      key = 'a' 
      id = {1}
      isLoop = {false}
      hotKey = 'q'
      sampleFile = {trigg} 
      keysDown = {new Set('a')}
      toggleLoop = {toggleLoop}  
    />)
    wrapperPadActive = mount(<Pad
      children = {[]}
      key = 'a' 
      id = {1}
      isLoop = {false}
      hotKey = 'q'
      sampleFile = {trigg} 
      keysDown = {new Set('q')}
      toggleLoop = { toggleLoop }  
    />)
  });

    it('Passes', () => { 
      expect(true).toEqual(true); 
    });

    it('has correct html elements', () => {
      const nodeRef = jest.fn();
      const toggleLoopMock = jest.fn();
      const padHitStyling = <div className = "PadIsHit"></div>;
      const padIdTag = <p className = "padId">pad # 1</p>;
      const sampleFileTag = <p className="sampleFile">{trigg}</p>;
      //  <p className = "sampleFile">/static/media/mattel-trigg.419fba59.wav</p>;
      const audioTag = <audio ref ={ nodeRef } controls loop={ false } src = { trigg } />;
      const buttonTag =  <button onClick = {()=>toggleLoopMock}>loop</button>
      // wrapper.find('.btn').simulate('click');
      // expect(wrapperPadActive.contains(padHitStyling)).toEqual(true);
      // console.log(padHitStyling);
      expect(wrapperPadActive.contains(padIdTag)).toEqual(true);
      expect(wrapperPadActive.contains(sampleFileTag)).toEqual(true);
      expect(wrapperPadActive.contains(audioTag)).toEqual(true);
    //  expect(wrapperPadActive.contains(buttonTag)).toEqual(true);



    } )

    it('loop button toggles isLoop',()=>{
        wrapperPadActive.find('#loopButton').simulate('click');
        expect(toggleLoop).toBeCalled(); 
        expect(toggleLoop).toHaveBeenLastCalledWith(1);
    })

    it('audio is playing if keys down includes hotKey',()=>{
      wrapperPadActive.find('#audio').expect('paused').toEqual(false);
      wrapperPadInactive.find('#audio').expect('paused').toEqual(true);
      // expect(toggleLoop).toBeCalled(); 
      // expect(toggleLoop).toHaveBeenLastCalledWith(1);
    })

  }); 
